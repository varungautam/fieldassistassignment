using System;
using System.Collections.Generic;


namespace OnilneShop
{
    class SecondaryCategory : ISchemeAttachable
    {
    
      //ISchemeAttachable properties
      public List<IScheme> schemeAttached {get; set;}
      
      //SecondaryCategory Public properties
      public string secondaryCategoryID { get; set; }
      public PrimaryCategory primaryCategory { get; set; }
      public string secondaryCategoryDetails { get; set;}
      
      //constructors
      public SecondaryCategory()
      {
      schemeAttached = new List<IScheme>();
      }
      
      public SecondaryCategory(string secondaryCatID, PrimaryCategory primaryCat, string secondaryCatDetails)
      {
      schemeAttached = new List<IScheme>();
      secondaryCategoryID = secondaryCatID;
      primaryCategory = primaryCat;
      secondaryCategoryDetails = secondaryCatDetails;
      }
      
      //Public Methods
      public void ChangePrimaryCategoryID(PrimaryCategory primaryCat)
      {
      primaryCategory = primaryCat;
      }
      
    }
}